'use strict'

const events = require('./constants')
const { posix: path } = require('path')

const RESOURCE_ID_RX = /^([^@:$]*)@(?:([^@:$]*):([^@:$]*))?:$/
const RESOURCE_ID_RX_GROUP = { version: 1, component: 2, module: 3 }
const SPACE_RX = / /g

module.exports.register = (eventEmitter, config = {}) => {
  const filters = config.map ? config.map.map(({ match, target }) => {
    const filter = []
    {
      const matcher = match.match(RESOURCE_ID_RX)
      matcher || console.warn(`invalid syntax for match ${match}: expected '@::'`)
      const version = matcher[RESOURCE_ID_RX_GROUP.version]
      version && (filter.push(['version', version]))
      const component = matcher[RESOURCE_ID_RX_GROUP.component]
      component && (filter.push(['component', component]))
      const module = matcher[RESOURCE_ID_RX_GROUP.module]
      module && (filter.push(['module', module]))
    }
    const map = {}
    {
      const match = target.match(RESOURCE_ID_RX)
      match || console.warn(`invalid syntax for target ${target}: expected '@::'`)
      const version = match[RESOURCE_ID_RX_GROUP.version]
      version && (map.version = version)
      const component = match[RESOURCE_ID_RX_GROUP.component]
      component && (map.component = component)
      const module = match[RESOURCE_ID_RX_GROUP.module]
      module && (map.module = module)
    }

    return [filter, map]
  }) : []

  var htmlUrlExtensionStyle

  eventEmitter.on(events.AFTER_CLASSIFY_CONTENT,
    (playbook, contentCatalog) => {
      htmlUrlExtensionStyle = (playbook.urls || {}).htmlExtensionStyle || 'default'

      contentCatalog.getAll().forEach((file) => {
        for (const [filter, map] of filters) {
          if (apply(file, filter, map, contentCatalog)) {
            break
          }
        }
      })
      return contentCatalog
    }
  )

  function apply (file, filter, map, contentCatalog) {
    if (match(file, filter)) {
      const src = file.src
      const family = src.family
      const actingFamily = family === 'alias' ? file.rel.src.family : family
      const target = Object.assign({}, src, {
        component: map.component ? map.component === '*' ? src.component : map.component : '',
        version: map.version ? map.version === '*' ? src.version === 'master' ? '' : src.version : map.version : '',
        module: map.module ? map.module === '*' ? src.module === 'ROOT' ? '' : src.module : map.module : '',
      })
      if (file.out) {
        file.out = computeOut(target, actingFamily)
      }
      if (file.pub) {
        const originalUrl = file.pub.url
        file.pub = computePub(target, file.out, actingFamily)
        const componentVersion = contentCatalog.getComponentVersion(src.component, src.version)
        if (originalUrl === componentVersion.url) {
          componentVersion.url = file.pub.url
        }
      }

      return true
    }
    return false
  }

  function match (file, filter) {
    const candidateSrc = file.src
    return (filter.every(([key, val]) => candidateSrc[key] === val))
  }

  function computeOut (src, family) {
    const component = src.component
    const version = src.version
    const module_ = src.module

    let basename = src.basename || path.basename(src.relative)
    const stem = src.stem || basename.substr(0, (basename.lastIndexOf('.') + 1 || basename.length + 1) - 1)
    if (src.mediaType === 'text/asciidoc') basename = stem + '.html'

    let indexifyPathSegment = ''
    if (family === 'page' && stem !== 'index' && htmlUrlExtensionStyle === 'indexify') {
      basename = 'index.html'
      indexifyPathSegment = stem
    }

    let familyPathSegment = ''
    if (family === 'image') {
      familyPathSegment = '_images'
    } else if (family === 'attachment') {
      familyPathSegment = '_attachments'
    }

    const modulePath = path.join(component, version, module_)
    const dirname = path.join(modulePath, familyPathSegment, path.dirname(src.relative), indexifyPathSegment)
    const path_ = path.join(dirname, basename)
    const moduleRootPath = path.relative(dirname, modulePath) || '.'
    const rootPath = path.relative(dirname, '') || '.'

    return { dirname, basename, path: path_, moduleRootPath, rootPath }
  }

  function computePub (src, out, family) {
    const pub = {}
    let url
    if (family === 'nav') {
      const urlSegments = [src.component]
      if (src.version !== 'master') urlSegments.push(src.version)
      if (src.module && src.module !== 'ROOT') urlSegments.push(src.module)
      // an artificial URL used for resolving page references in navigation model
      url = '/' + urlSegments.join('/') + '/'
      pub.moduleRootPath = '.'
    } else if (family === 'page') {
      const urlSegments = out.path.split('/')
      const lastUrlSegmentIdx = urlSegments.length - 1
      if (htmlUrlExtensionStyle === 'drop') {
        // drop just the .html extension or, if the filename is index.html, the whole segment
        const lastUrlSegment = urlSegments[lastUrlSegmentIdx]
        urlSegments[lastUrlSegmentIdx] =
          lastUrlSegment === 'index.html' ? '' : lastUrlSegment.substr(0, lastUrlSegment.length - 5)
      } else if (htmlUrlExtensionStyle === 'indexify') {
        urlSegments[lastUrlSegmentIdx] = ''
      }
      url = '/' + urlSegments.join('/')
    } else {
      url = '/' + out.path
    }

    pub.url = ~url.indexOf(' ') ? url.replace(SPACE_RX, '%20') : url

    if (out) {
      pub.moduleRootPath = out.moduleRootPath
      pub.rootPath = out.rootPath
    }

    return pub
  }
}
