/* eslint-env mocha */
'use strict'

const EventEmitter = require('events')
const pubMap = require('./../lib/pub-map')
const events = require('./../lib/constants')
const ContentCatalog = require('@antora/content-classifier/lib/content-catalog')
const chai = require('chai')
chai.use(require('chai-string'))

describe('pub-map tests', () => {
  var eventEmitter

  function setUpEventEmitter () {
    const baseEmitter = new EventEmitter()

    eventEmitter = {

      emit: async (name, ...args) => {
        const promises = []
        baseEmitter.emit(name, promises, ...args)
        promises.length && await Promise.all(promises)
      },

      on: (name, listener) => baseEmitter.on(name, (promises, ...args) => promises.push(listener(...args))),
    }
  }

  var contentCatalog
  var playbook
  var originals

  beforeEach(() => {
    setUpEventEmitter()
    contentCatalog = new ContentCatalog()
    playbook = {}
    originals = {}
    ;['c1', 'c2'].forEach((component) => {
      const versions = {}
      originals[component] = versions
      ;['1.0', '2.0', 'master'].forEach((version) => {
        contentCatalog.registerComponentVersion(component, version)
        const modules = {}
        versions[version] = modules
        ;['ROOT', 'm1', 'm2'].forEach((module) => {
          const pages = {}
          modules[module] = pages
          ;['index', 'p1', 'p2'].forEach((page) => {
            const file = contentCatalog.addFile({
              src: {
                component,
                version,
                module,
                family: 'page',
                relative: `${page}.adoc`,
                stem: page,
                mediaType: 'text/asciidoc',
              },
            })
            pages[page] = [file.out, file.pub]
          }
          )
        }
        )
      })
    })
  })

  function checkOriginals (contentCatalog, originals) {
    Object.entries(originals).forEach(([component, c]) =>
      Object.entries(c).forEach(([version, v]) =>
        Object.entries(v).forEach(([module, m]) =>
          Object.entries(m).forEach(([page, data]) => {
            const files = contentCatalog.findBy({
              component,
              version,
              module,
              relative: `${page}.adoc`,
            })
            chai.expect(files.length).to.equal(1)
            chai.expect(files[0].out).equals(data[0])
            chai.expect(files[0].pub).equals(data[1])
          }))))
  }

  function checkPages (pages, path) {
    const pubPath = path === '.' ? '/' : '/' + path + '/'
    pages.forEach((page) => {
      chai.expect(page.out.dirname).equals(path)
      chai.expect(page.pub.url).to.startsWith(pubPath)
    })
  }

  it('no config does nothing', () => {
    pubMap.register(eventEmitter, {})
    eventEmitter.emit(events.AFTER_CLASSIFY_CONTENT, playbook, contentCatalog)
    checkOriginals(contentCatalog, originals)
  })

  it('map c1:ROOT to blank', () => {
    pubMap.register(eventEmitter, { map: [{ match: '@c1:ROOT:', target: '*@:*:' }] })
    eventEmitter.emit(events.AFTER_CLASSIFY_CONTENT, playbook, contentCatalog)
    checkPages(contentCatalog.findBy({ component: 'c1', version: '1.0', module: 'ROOT' }), '1.0')
    checkPages(contentCatalog.findBy({ component: 'c1', version: '2.0', module: 'ROOT' }), '2.0')
    checkPages(contentCatalog.findBy({ component: 'c1', version: 'master', module: 'ROOT' }), '.')
    delete originals.c1['1.0'].ROOT
    delete originals.c1['2.0'].ROOT
    delete originals.c1.master.ROOT
    checkOriginals(contentCatalog, originals)
  })

  it('map c1:m1 to blank', () => {
    pubMap.register(eventEmitter, { map: [{ match: '@c1:m1:', target: '*@::' }] })
    eventEmitter.emit(events.AFTER_CLASSIFY_CONTENT, playbook, contentCatalog)
    checkPages(contentCatalog.findBy({ component: 'c1', version: '1.0', module: 'm1' }), '1.0')
    checkPages(contentCatalog.findBy({ component: 'c1', version: '2.0', module: 'm1' }), '2.0')
    checkPages(contentCatalog.findBy({ component: 'c1', version: 'master', module: 'm1' }), '.')
    delete originals.c1['1.0'].m1
    delete originals.c1['2.0'].m1
    delete originals.c1.master.m1
    checkOriginals(contentCatalog, originals)
  })

  it('map c1 to blank', () => {
    pubMap.register(eventEmitter, { map: [{ match: '@c1::', target: '*@::' }] })
    eventEmitter.emit(events.AFTER_CLASSIFY_CONTENT, playbook, contentCatalog)
    ;['ROOT', 'm1', 'm2'].forEach((module) => {
      checkPages(contentCatalog.findBy({ component: 'c1', version: '1.0', module }), '1.0')
      checkPages(contentCatalog.findBy({ component: 'c1', version: '2.0', module }), '2.0')
      checkPages(contentCatalog.findBy({ component: 'c1', version: 'master', module }), '.')
    })
    delete originals.c1['1.0']
    delete originals.c1['2.0']
    delete originals.c1.master
    checkOriginals(contentCatalog, originals)
  })

  it('map m1 to a/b/c/1', () => {
    pubMap.register(eventEmitter, { map: [{ match: '@:m1:', target: '*@*:a/b/c/1:' }] })
    eventEmitter.emit(events.AFTER_CLASSIFY_CONTENT, playbook, contentCatalog)
    ;['c1', 'c2'].forEach((component) => {
      checkPages(contentCatalog.findBy({ component, version: '1.0', module: 'm1' }), `${component}/1.0/a/b/c/1`)
      checkPages(contentCatalog.findBy({ component, version: '2.0', module: 'm1' }), `${component}/2.0/a/b/c/1`)
      checkPages(contentCatalog.findBy({ component, version: 'master', module: 'm1' }), `${component}/a/b/c/1`)
      delete originals[component]['1.0'].m1
      delete originals[component]['2.0'].m1
      delete originals[component].master.m1
    })
    checkOriginals(contentCatalog, originals)
  })
})
